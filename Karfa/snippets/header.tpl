<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>{$title_for_layout}</title>
<base href="{$baseurl}" />

<!-- CSS -->
{"main.css"|asset_url|stylesheet_tag}
{"karfa.css"|asset_url|stylesheet_tag}
{"print.css"|asset_url|stylesheet_tag}
{"typography.css"|asset_url|stylesheet_tag}

<!-- Javascript -->
{"jquery.js"|asset_url|script_tag}
{"karfa.js"|asset_url|script_tag}

</head>

<body>
<div id="loading"><div class="indicator"></div></div>
<div id="wrapper">

	<div id="fullheader">
		<div class="currency">
				{"Currency"|translate}: {switch_currency}
		</div>
		
		<div id="shopping-cart">
		<div class="shopping-cart-inner">
		<a href="cart" class="cart1">{img_tag name="cart1.png" alt=""}</a>
	  <div class="cart2">
	  {if ($cart.cart_line_count > 1)}
	  <p>You have <a href="cart">{$cart.cart_line_count}</a> {"products in your cart"|translate}.</p>
        {/if}
        {if ($cart.cart_line_count == 1)}
	  <p>You have <a href="cart">{$cart.cart_line_count}</a> {"product in your cart"|translate}.</p>
        {/if}
        {if ($cart.cart_line_count == 0)}
	  <p>{"Your cart is empty"|translate}</p>
        {/if}
        </div>
        
        <a href="checkout" class="cart3">{"Go to Checkout"|translate}</a>
        
		</div>
		</div>
		
		<div class="logo">
			<a href="/" title="{"Go to Homepage"|translate}" class="tooltip">{logo_tag}</a>
		</div>
		
	</div>