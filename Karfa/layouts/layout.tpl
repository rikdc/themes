<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>{$title_for_layout}</title>
<base href="{$baseurl}" />


{if $template|contains:"product"}
<meta property="og:image" content="{section name=image loop=$product.images}{if $smarty.section.image.first}{$product.images[image].name|product_image_url}{/if}{/section}"/>
{/if}

<html xmlns:fb="http://ogp.me/ns/fb#">

<!-- CSS -->
{"main.css"|asset_url|stylesheet_tag}
{"karfa.css"|asset_url|stylesheet_tag}
{"bottom.css"|asset_url|stylesheet_tag}
{"superfish.css"|asset_url|stylesheet_tag}
{"superfish-vertical.css"|asset_url|stylesheet_tag}
{"typography.css"|asset_url|stylesheet_tag}
{"jquery.fancybox.css"|asset_url|stylesheet_tag}
{"jquery.fancybox-buttons.css"|asset_url|stylesheet_tag}
{"jquery.fancybox-thumbs.css"|asset_url|stylesheet_tag}

<!-- Javascript -->

{"jquery-1.7.min.js"|asset_url|script_tag}
{"jquery.easing.1.3.min.js"|asset_url|script_tag}
{"trans-banner.min.js"|asset_url|script_tag}
{"jquery.tipsy.js"|asset_url|script_tag}
{"jquery.innerfade.js"|asset_url|script_tag}
{"hoverIntent.js"|asset_url|script_tag}
{"superfish.js"|asset_url|script_tag}
{"jquery.mousewheel-3.0.6.pack.js"|asset_url|script_tag}
{"jquery.fancybox.js"|asset_url|script_tag}
{"jquery.fancybox-buttons.js"|asset_url|script_tag}
{"jquery.fancybox-thumbs.js"|asset_url|script_tag}
{"karfa.js"|asset_url|script_tag}

</head>

<body>
<div id="fb-root"></div>

{literal}
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/is_IS/all.js#xfbml=1&appId=157590797676302";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

{/literal}
<div id="loading"><div class="indicator"></div></div>
<div id="wrapper">

	<div id="fullheader">
		<div class="currency">
				{"Currency"|translate}: {switch_currency}
		</div>
		
		
		<div id="header-top-nav">
		<a href="cart" class="cart3">{"View Cart"|translate}</a> | <a href="checkout" class="cart3">{"Checkout"|translate}</a>
		</div>		
		{if $site.facebook_url == ""}{else}
		<a href="{$site.facebook_url}" target="_blank" class="facebookimg tooltip" title="{"We are on Facebook"|translate}"></a>
		{/if}
		<div id="shopping-cart">
		<div class="shopping-cart-inner">
		<a href="cart" title="Skoða Körfu" class="cart1 tooltip">{img_tag name="cart1.png"|asset_url}</a>
	  <div class="cart2">
	  {if ($cart.cart_line_count > 1)}
	  <p>{"You have"|translate} <a href="cart">{$cart.cart_line_count}</a> {"items in your cart"|translate}.</p>
        {/if}
        {if ($cart.cart_line_count == 1)}
	  <p>{"You have"|translate} <a href="cart">{$cart.cart_line_count}</a> {"item in your cart"|translate}.</p>
        {/if}
        {if ($cart.cart_line_count == 0)}
	  <p>{"Your cart is empty"|translate}</p>
        {/if}
        </div>
        
       
        
        
		</div>
		</div>
		
		<div class="logo">
			<a href="/" title="{"Go to Homepage"|translate}" class="tooltip">{logo_tag}</a>
		</div>
		
		<div class="searchform">
		<form action="/search" method="get" name="srchfrm" >
	<input class="searchfield" type="text" name="q" value="{"Search..."|translate}" onfocus="{literal}if (this.value == '{"Search..."|translate}') {this.value = '';}{/literal}" onblur="{literal}if (this.value == '') {this.value = 'Leita…';}{/literal}" />
	<input type="button" class="hide" value="{"Search"|translate}" />
</form></div>
		
	</div>


	<div id="content-wrapper">
	<div id="stripe">
	<ul><li>
		<a href="/">{"Home"|translate}</a>
	</li>
	</ul>
		{pages_menu}
		
		<ul><li>
			<a href="/contact">{"Contact Us"|translate}</a>
		</li>
		</ul>
	</div>
	<div id="main">
		<div id="main-content-area" class="shadow-left">
		{if $isHome}
		<div id="slideshow">
			{slideshow name="slideshow" width="720" height="350" crop="Yes" scale="No"}
		</div>
		

		{else}
		{/if}
		<br style="clear: both;" />
		
		{customer_messages}
		{$content_for_layout}
		<br style="clear: both;" />
		</div>
		
		<div id="sidebar" class="left">
		<h3>{"Categories"|translate}<span class="h3arrow"></span></h3>
		<br style="clear: both;" />
		<div id="categories" class="vertical">
			{categories_menu}
		</div>

	<br style="clear: both;" />
	<h3>{"Customers"|translate} <span class="h3arrow"></span></h3>

	{if $customer}
	<br style="clear: both;" />
	<ul id="side-clients">
		<li>
			<a href="/customers/orders">{"Orders History"|translate}</a>
		</li>
		<li>
			<a href="/customers/edit">{"My Account"|translate}</a>
		</li>
		<li>
			<a href="/customers/logout">{"Logout"|translate}</a>
		</li>
	</ul>
	
	{else}
	<br style="clear: both;" />
	<ul id="side-clients">
		<li>
			<a href="/customers/login">{"Login"|translate}</a>
		</li>
		<li>
			<a href="/customers/register">{"Register"|translate}</a>
		</li>
	</ul>
	
	{/if}
	
	<br style="clear: both;" />
	<h3>{"New Products"|translate} <span class="h3arrow"></span></h3>
	<br style="clear:both;" />
	<ul id="new-products">
	{foreach from=$site.products.new item=product}
		<li>
		<div class="new-product-image">
		{section name=image loop=$product.images}
			{if $smarty.section.image.first}
				{img_tag name=$product.images[image].name|product_image_url:'thumb'}
			{/if}
		{/section}
		</div>
		<a href="products/{$product.slug}">{$product.name}</a>
	   </li>
	{/foreach}
	</ul>
	
	{if $site.products.recent} 
	
	<h3>{"Recently Viewed"|translate} <span class="h3arrow"></span></h3>
	<br style="clear:both;" />
	<ul id="recently-viewed">
	{foreach from=$site.products.recent item=product}
		<li>
		
		<div class="recently-viewed-image">
			{section name=image loop=$product.images}
				{if $smarty.section.image.first}
					{img_tag name=$product.images[image].name|product_image_url:'thumb'}
				{/if}
			{/section}
		</div>
		
		<a href="products/{$product.slug}">{$product.name}</a>
	   </li>
	{/foreach}
	</ul>
	
	{/if}
	
	
	</div>
	<br style="clear: both;" /><br />
	<div id="footer">
		
		{if $site.facebook_url == ""}{else}
		<div id="footbox">
		<fb:like-box href="{$site.facebook_url}" width="940" show_faces="true" border_color="white" stream="false" header="false"></fb:like-box>
		</div>
		{/if}
		
	</div>
	</div>
	
	</div>
	
	<img src="{"layout_bottom.png"|asset_url}" />
	
</div>
<div id="footer-content">
{"Copyright"|translate} &copy; <strong>{$siteName}</strong> {$yesterday|date_format:"%Y"} {"All Rights Reserved"|translate} - <a href="mailto:{$site.email}">{$site.email}</a> - {"Telephone"|translate}: {$site.phone} - {$site.address}, {$site.postcode} {$site.city}<br /><br />

<a href="http://www.karfa.is/" target="_blank"><img src="{"footerlogo.png"|asset_url}" /></a>
<br /><br />

</div>
{content_for_header}
</body>
</html>
