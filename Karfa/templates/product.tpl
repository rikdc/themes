<div class="product-wrapper">
<h1>{$product.name}</h1>

<div id="product-text">
<div class="product-social">
<div class="fb-like" data-href="http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}" data-send="true" data-layout="button_count" data-width="250" data-show-faces="false"></div>
</div>

	<p>{$product.description}</p>
	<br />
	{if $product.variations|@count gt 1}{"Select"|translate}:{else}<div class="price">{$product.price|money}</div>{/if}
<form action="carts/add" method="post">
    <input type="hidden" name="data[CartLine][quantity]" value="1" />
    <select id="product-select" name="data[CartLine][product_line_id]" {if $product.variations|@count lte 1} style='display:none;' {/if}>
    {section name=line loop=$product.variations}
        <option value="{$product.variations[line].id}">{$product.variations[line].name} - {$product.variations[line].price|money}</option>
    {/section}
    </select>
    
    <br style="clear: both;" /><br />
    <div class="submit">
      <button class="button blue" name="add" type="submit">{"Add to Cart"|translate}</button>
    </div>
    
</div>
</form>

<div id="product-gallery">

{section name=image loop=$product.images}
<a href="{$product.images[image].name|product_image_url:'grande'}" class="fancybox">
		{if $smarty.section.image.first}
			{img_tag name=$product.images[image].name|product_image_url:'medium'}
		{/if}
</a>
{/section}

<div id="smaller-images">
{section name=image loop=$product.images}	
<a href="{$product.images[image].name|product_image_url:'grande'}" data-fancybox-group="gallery" class="fancybox">
	{img_tag name=$product.images[image].name|product_image_url:'small'}
</a>
 {/section}
</div>
    
</div>
<br style="clear: both;" />
<br />
<h2>{"Related Products"|translate}</h2>
<div id="related-products">
	{foreach item=prod from=$product.related.manual}
	<b>{$prod.name}</b><br /> - {$prod.price}<br /><br />
	{/foreach}
</div>

<br style="clear: both;" />
{if $site.facebook_url == ""}{else}
<fb:comments href="http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}" num_posts="2" width="690"></fb:comments>
{/if}
<br />



<a href="#" onclick="history.back();return false;">{"Back"|translate}</a>
</div>