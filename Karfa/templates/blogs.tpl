<h2>{"Blog"|translate}</h2><br />
{if isset($posts)}
    {foreach from=$posts item=post}
        <div class="blog-entry">
        <span class="blog-date">{$post.Blog.updated|date_format:"%d/%m/%Y"}</span>
        <h4><a href="blogs/{$post.Blog.slug}">{$post.Blog.title}</a></h4>
        <div class="blog-content">{$post.Blog.content|truncate:400:true}... <a href="/blogs/{$post.Blog.slug}">{"Read More"|translate}</a></div> 
        </div>
    {/foreach}
{/if}