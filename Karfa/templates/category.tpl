<h1>{$category.title}</h1>
<p class="categoriesdescription">{$category.description}</p>

<br style="clear: both;" /><br />
<div class="products-search">

{foreach from=$products item=product}
<div class="item">

{if $product.is_new == "1"}
<div class="nytt">
	<img src="{"nytt.png"|asset_url}" />
</div>
{/if}
     		<div class="product-image">
            <a href="products/{$product.slug}" class=" tooltip center-img" title="{"View More"|translate}"> 
            
            	 {section name=image loop=$product.images}
       				{if $smarty.section.image.first}
    						{img_tag name=$product.images[image].name|product_image_url:'compact'}
       				{/if}
   			     {/section}
            
            </a>
             </div>
             <div>
             <a href="products/{$product.slug}" class="ptitle2 product_title1">{$product.name|truncate:75:"...":true}</a>
             </div>
             
            <div class="price">{$product.price|money}</div>
    		<div class="buy-button">
    		
    		{if $product.variations|@count gt 1}
    		                  <a href="/products/{$product.slug}" class="karfa-button">{"View More"|translate}</a>
    		              {else}
    		                  <form method="post" action="/carts/add" id="form{$product.slug}">
    		                      <input type="hidden" name="data[CartLine][quantity]" value="1" />
    		                      <input type="hidden" name="data[CartLine][product_line_id]" value="{$product.variations[0].id}" />
    		                      
    		                      <button class="karfa-button" name="add" type="submit">{"Add to Cart"|translate}</button>
    		                      
    		                  </form>
    		              {/if}
    		              
    		  
    		</div>
        </div>
{/foreach}

<br style="clear: both;" /><br />

{if (count($products) > 0)}
    <div class="pageControl">{pages}</div>
{/if}
</div>