<h2>{"Search Results"|translate}</h2>

<ul id="search-results">
{foreach item=result from=$search.results}
    <li><h4><a href="{$result.url}">{$result.name}</a></h4></li>
{/foreach}
</ul>