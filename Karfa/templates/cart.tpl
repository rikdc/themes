<h1>{"My Shopping Cart"|translate}</h1>

{if $cart.cart_line_count == 0}
{"Your cart is empty"|translate}
{else}
<div class="basket">
    <form action="carts/edit" method="post">
<table width="100%" cellpadding="0" cellspacing="0">
<thead>
    <tr>
    	<th class="productListing-heading">{"Product Name"|translate}</th>
    	<th class="productListing-heading">{"Quantity"|translate}</th>
    	<th class="productListing-heading">{"Price pr. item"|translate}</th>
    	<th class="productListing-heading">{"Products Total"|translate}</th>
    	<th class="productListing-heading">&nbsp;</th>
    </tr>
  </thead>
  <tbody>
	
    {foreach from=$cart.Lines item=line}
	<tr class="line">
    	<td>{$line.name}</td>
        <td><input size="3" value="{$line.quantity}" name="data[CartLine][{$line.product_line_id}][quantity]" /></td>
        <td>{$line.price|money}</td>
        <td>{$line.subtotal|money}</td>
        <td><a href="carts/remove/{$line.id}">{"Delete"|translate}</a></td>    
    </tr>    
    {/foreach}
    <tr>
        <td align="right" colspan="3" class="cart_total"><span class="b">{"Tax"|translate}:</span></td>
        <td>{$cart.tax|money}</td>
        <td></td>
    </tr>
    <tr>
        <td align="right" colspan="3" class="cart_total"><span class="b">{"Subtotal"|translate}:</span></td>
        <td>{$cart.subtotal|money}</td>
        <td></td>
    </tr>
    </tbody>
</table>
</div>
<br /><br />

   <div style="clear:both">
   <br /><br />
	
      <div style="float:left">
   		<input type="submit" name="submit" class="button" value="{"Update Cart"|translate}" /> 
      </div>
   <div style="float:right;">
        <a href="checkout/" class="button blue">{"Continue to Checkout"|translate}</a>
   </div>
   <div style="float:right; margin-right:10px;">
        <a href="/" class="button blue">{"Continue Shopping"|translate}</a>
   </div>
   </div>
</form>
{/if}