<h1>Your cart</h1>

<div class="basket">
    <form action="carts/edit" method="post">
<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
    	<th class="productListing-heading">Cart Items</th>
    	<th class="productListing-heading">Qty</th>
    	<th class="productListing-heading">Item Price</th>
    	<th class="productListing-heading">Item Total</th>
    	<th class="productListing-heading">&nbsp;</th>
    </tr>
    {foreach from=$cart.Lines item=line}
	<tr class="line">
    	<td>{$line.name}</td>
        <td><input size="3" value="{$line.quantity}" name="data[CartLine][{$line.product_line_id}][quantity]" /></td>
        <td>{$line.line_subtotal|money}</td>
        <td>{$line.line_total|money}</td>
        <td><a href="carts/remove/{$line.id}">remove</a></td>    
    </tr>    
    {/foreach}
    <tr>
        <td align="right" colspan="3" class="cart_total"><span class="b">TAXES:</span></td>
        <td>{$cart.tax|money}</td>
        <td></td>
    </tr>
    <tr>
        <td align="right" colspan="3" class="cart_total"><span class="b">TOTAL:</span></td>
        <td>{$cart.subtotal|money}</td>
        <td></td>
    </tr>
</table>
</div>

   <div style="clear:both">
      <div style="float:left">
   		<input type="submit" name="submit" value="Update Cart" /> 
      </div>
   <div style="float:right;">
        <a href="checkout/">CHECKOUT</a>
   </div>
   </div>
</form>