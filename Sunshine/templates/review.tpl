<h1>Please review your order</h1>

<p>Once you're happy with your order, click on "Proceed to Checkout" to pay via Paypal. Alternatively, if you wish 
to amend your order, click "Back" and you'll return to your cart where you can make adjustments to your items</p>

<div class="basket">
    <form action="carts/wizard/review" method="post">
<table width="100%" cellpadding="0" cellspacing="0">
	<tr>
    	<th class="productListing-heading">Cart Items</th>
    	<th class="productListing-heading">Qty</th>
    	<th class="productListing-heading">Item Price</th>
    	<th class="productListing-heading">Item Total</th>
    </tr>
    {foreach from=$cart.Lines item=line}
	<tr class="line">
    	<td>{$line.name}<br/>{$line.product_line_name}</td>
        <td>{$line.quantity}</td>
        <td>{$line.line_subtotal|money}</td>
        <td>{$line.line_total|money}</td>
    </tr>    
    {/foreach}
    <tr>
        <td align="right" colspan="3" class="cart_total"><span class="b">TOTAL:</span></td>
        <td>{$cart.total|money}</td>
        <td></td>
    </tr>
    <tr>
        <td align="right" colspan="3" class="cart_total"><span class="b">TAXES:</span></td>
        <td>{$cart.tax|money}</td>
        <td></td>
    </tr>
    <tr>
        <td align="right" colspan="3" class="cart_total"><span class="b">SHIPPING COSTS:</span></td>
        <td>{$cart.shipping|money}</td>
        <td></td>
    </tr>
    <tr>
        <td align="right" colspan="3" class="cart_total"><span class="b">SUBTOTAL:</span></td>
        <td>{$cart.subtotal|money}</td>
        <td></td>
    </tr>
</table>
</div>

<div style="clear:both">
      <div style="float:right">
           <a href='carts/view'>BACK</a>
      </div>
      <div style="float:left">
        <input type='hidden' name='payment' value='yes' />
		<input type="submit" name="submit" value="Proceed to Payment" /> 
      </div>
</form>
</div>
    