<h1>{$category.title}</h1>
<p class="categoriesdescription">{$category.description}</p>
{if (count($categories) > 0)}
    <div style="border-bottom:1px solid #d1d1d1">
        <p>Select a sub-category:</p>
        <ul>
            {foreach from=$categories item=category}
                <li><a href="{$category.url}">{$category.title}</a></li>
            {/foreach}
        </ul>
    </div>
{/if}

{if (count($products) > 0)}
    <div class="pageControl">{pages}</div>
{/if}
    
<div class="clear"></div>
<div class="products-search">
{foreach from=$products item=product}
	<div class="product">
	    <div class="left">
		    {section name=image loop=$product.images}
		        {if $smarty.section.image.first}
		        <div class="product-image">
                            <a href="products/{$product.slug}">
					<img src="{$product.images[image].name|image_resize:125x125}" title="{$product.images[image].title}" />
			    </a>
                        </div>
		        {/if}
			{/section}
	    </div>
	    <div class="left">
		    <a class='product-title' href="products/{$product.slug}">{$product.name|truncate:75:"...":true}</a>	    
	        <div class="product-price">&pound;{$product.price | money}</div>
	        <div class="product-description">{$product.description|truncate:300:"...":true}</div>
            <div class="product-readmore"><a href="products/{$product.slug}">More...</a></div>
	    </div>
	    <div class="clear"></div>
	</div>
{/foreach}
{if (count($products) > 0)}
    <div class="pageControl">{pages}</div>
{/if}
</div>