<h1>{$blog.title}</h1>
{$blog.content}

<h2>Categories</h2>
{if isset($cats)}
<ul>
    {foreach from=$cats item=category}
        <li>{link title=$category.Page.title url=$category.Page.url}</li>
    {/foreach}
</ul>
{/if}

<h2>Posts</h2>
    {foreach from=$posts item=post}
    <div class='blog-post-item'>
        <h3 class='blog-post-title'>{$post.title}</h3>
        <div class='blog-post-date'>{$post.created|relative_date}</div>
        <div class='blog-post-content'>{$post.content}</div>
    </div>
    {/foreach}