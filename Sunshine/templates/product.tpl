<h1 class="product">{$product.name}</h1>
<div class="product-header">
    {section name=image loop=$product.images}
        {if $smarty.section.image.first}
        <div class="product-image">
	    <img src="{img_tag name=$product.images[image].name size="thumb"}" title="{$product.images[image].title}" />
	</div>
        {/if}
    {/section}
</div>
<div class="product-details">
    <p class="details">{$product.description}</p>
    <div class="price">{if $product.variations|@count gt 1}Select from...{else}{$product.price|money}{/if}</div>
<form action="carts/add" method="post">
    <input type="hidden" name="data[CartLine][quantity]" value="1" />
    <select id="product-select" name="data[CartLine][product_line_id]" {if $product.variations|@count lte 1} style='display:none;' {/if}>
    {section name=line loop=$product.variations}
        <option value="{$product.variations[line].id}">{$product.variations[line].name} - {$product.variations[line].price|money}</option>
    {/section}
    </select>
    
    <div class="submit">
      <input type="submit" name="add" value="Add to Cart" />
    </div>

    <div class="product-tellafriendlink">
      <a href='{$baseurl}/products/tell_a_friend/{$product.slug}'>Tell a friend about this product</a>
    </div>
</form>
</div>