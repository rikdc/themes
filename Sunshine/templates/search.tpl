<h2>Search Results</h2>

<p style="margin-bottom:10px">Showing results for <a href="/search?{$search.query}">{$search.query}</a></p>

{pages}

{foreach item=result from=$search.results}
    <a href="{$result.url}">{$result.name}</a>
{/foreach}