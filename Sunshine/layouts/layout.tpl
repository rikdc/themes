<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>{$title_for_layout}</title>
    <base href="{$baseurl}" />
    {stylesheet name="default.css"}
    {content_for_header}
{literal} 
<script>
<!--

sfHover = function() {
	var sfEls = document.getElementById("nav").getElementsByTagName("LI");
	for (var i=0; i<sfEls.length; i++) {
		sfEls[i].onmouseover=function() {
			this.className+=" sfhover";
		}
		sfEls[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
		}
	}
}
if (window.attachEvent) window.attachEvent("onload", sfHover);

-->
</script>
{/literal} 
</head>

<body>
<div id="container">
  <div id="header">
    <div class="content">
      <h1>Site Name Here</h1>
      <h2>Site tagline here</h2>
      <div id="logo">
    
      </div> <!-- end #logo -->
      <div class='loginbox'>
    
      </div>          
    </div> <!-- end .content -->
    <div id="nav" class="navigation">
      <ul>
        <li class='short'><a href='{$baseurl}'>Home</a></li>
        <li><a href='#'>All Categories</a>
{build_menu name=Categories}
        </li>
        <li><a href='#'>Browse Products</a>
{build_menu name=Special}
        </li>
        <li class='short'><a href='{$baseurl}pages/about-us'>About Us</a></li>
      </ul>
    </div> <!-- end .navigation -->
  </div> <!-- end #header -->
  <div id="body">
    <div class="navigation">
      <div class='navigation_box'>
        
        <div class="dialog">
            <div class="dialog_content">
                <div class="t"></div>
        
        <h3>Your Cart</h3>
        {if ($cart.cart_line_count > 0)}
          <p>You have {$cart.cart_line_count} item(s) in your cart. </p>
          <p><a href="cart">View Cart</a></p>
        {else}
          <p>Your cart is currently empty.</p>
        {/if}
        
                </div>
            <div class="b"><div></div></div>
        </div>
        
      </div>
      <div class='navigation_box'>
        
        <div class="dialog">
            <div class="dialog_content">
                <div class="t"></div>

                {customer_panel}
          
                </div>
            <div class="b"><div></div></div>
        </div>
        
      </div>

      
      <div class='navigation_box'>
        
        <div class="dialog">
            <div class="dialog_content">
                <div class="t"></div>

                <h3>Information</h3>
                {build_menu name=Pages}
                </div>
            <div class="b"><div></div></div>
        </div>
        
      </div>
      
      <div class='navigation_box'>
        
        <div class="dialog">
            <div class="dialog_content">
                <div class="t"></div>

                <h3>Currency</h3>
                <p>Select your preferred currency:</p>
                {switch_currency}
                </div>
               
            <div class="b"><div></div></div>
        </div>
        
      </div>
      
    </div> <!-- end .navigation -->
    <div class="content">
        {$layout->flash()}
        {breadcrumbs}
        <div id="maincontent">
          {$content_for_layout}
        </div> 
    </div> <!-- end .content -->
  </div> <!-- end #body -->
  <div class='clear'></div>  
  <div id="footer">
    <div class="content">
      {snippet name="footer"}
    </div> <!-- end .content -->
  </div> <!-- end #footer -->

</div>
</body>

</html>