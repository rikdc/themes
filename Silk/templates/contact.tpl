<div class="product-wrapper">
<h1>{"Contact Us"|translate}</h1>
<br style="clear: both;" />
<div id="contact-details">
		<h3><span class="symbol">@</span> {$siteName}</h3>
		<p class="description">{$site.description}</p>
		<p><span></span><strong>{"Telephone"|translate}:</strong> {$site.phone}</p>
		<p><span></span><strong>{"Address"|translate}:</strong> {$site.address}, {$site.postcode} {$site.city}</p>
		<p><span></span><strong>{"Email"|translate}:</strong> <a href="mailto:{$site.email}">{$site.email}</a></p>
		<h3>{"Our Location"|translate}</h3>
		<iframe frameborder="0" width="100%" height="300" src="http://maps.yahoo.com/embed#q={$site.address}+{$site.postcode}+{$site.city}&conf=1&zoom=14&mvt=m&trf=0"></iframe>
</div>
<div id="contant-form">
<p>{"Please complete and submit the following form. Our aim is to respond to all inquiries within few hours. We respect your desire for privacy and will not share your e-mail address with third parties."|translate}</p>
{contact_form}
</div>
</div>