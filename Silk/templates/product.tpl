<div class="product-wrapper">
<h1>{$product.name}</h1>

<div id="product-text">

	<div class="span3" id="product-gallery">

{section name=image loop=$product.images}
{if $smarty.section.image.first}
<a class="thumbnail" href="{$product.images[image].name|product_image_url:'grande'}" class="fancybox">
{img_tag name=$product.images[image].name|product_image_url:'medium'}</a>
{/if}
{/section}

<div id="smaller-images">
{section name=image loop=$product.images}	
<a href="{$product.images[image].name|product_image_url:'grande'}" data-fancybox-group="gallery" class="fancybox thumbnail">
	{img_tag name=$product.images[image].name|product_image_url:'small'}
</a>
 {/section}
</div>
    
</div>


	
	<div class="span9 product-info">
	{if $site.facebook_url == ""}{else}
	<div class="facebook-like">
	<div class="fb-like" data-href="http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}" data-send="true" data-layout="button_count" data-width="200" data-show-faces="false"></div>
	</div>
	{/if}
	
	<ul class="nav nav-tabs tabs">
  <li class="active"><a href="#description"><span class="symbol basecolor">,</span> {"Product Description"|translate}</a></li>
  {if $site.facebook_url == ""}{else}
  <li><a href="#reviews"><span class="symbol basecolor">c</span> {"Product Reviews"|translate}</a></li>
  {/if}
</ul>
 
<div class="tab-content">
  <div class="tab-pane active" id="description">
	  <p>{$product.description}</p>
  </div>
  <div class="tab-pane" id="reviews">
	  {if $site.facebook_url == ""}{else}
	  	<fb:comments href="http://{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}" num_posts="5" width="680"></fb:comments>
	  {/if}
  </div>
</div>
	
	
	{if $product.variations|@count gt 1}<div class="detail-price"><span>{"Price"|translate}</span>: ({"select below"|translate})</div> {else}<div class="detail-price"><span>{"Verð"|translate}</span>: {$product.price|money} 
	
	{if ($product.was_price > 0)}
	<b>{"was"|translate}</b> <span class="price-was-details">{$product.was_price|money}</span>
	{else}{/if}
	
	</div>{/if}
	
    <form action="carts/add" method="post">
    
    {if $product.variations|@count lte 1}<div style="display: none;">{/if}
    <select id="product-select" name="data[CartLine][product_line_id]">
    {section name=line loop=$product.variations}
        <option value="{$product.variations[line].id}">{$product.variations[line].name} - {$product.variations[line].price|money}</option>
    {/section}
    </select>
    {if $product.variations|@count lte 1}</div>{/if}
    
    <br style="clear: both;" /><br />
    
    <div class="input-prepend input-append">
    	<span class="add-on">{"Qty"|translate}: </span>
	    <input class="span1" id="appendedInputButton" name="data[CartLine][quantity]" value="1" size="16" type="text">
	    <button class="btn btn-primary" name="add" type="submit"><i class="icon-left icon-shopping-cart icon-white"></i> {"Add to Cart"|translate}</button>
    </div>

</div>
</form>
</div>

<br style="clear: both;" />
<br />

{if !empty($product.related.manual)}<h3>{"Related Products"|translate}</h3>{/if}
<div id="related-products">
<ul>
	{foreach item=prod from=$product.related.manual}
	<li>
	<a class="thumbnail" href="{$prod.url}">
	{section name=image loop=$product.related.manual}
	
{if $smarty.section.image.first}

{img_tag name=$product.images[image].name|product_image_url:'medium'}
{/if}
{/section}
	</a>
	
	<b>{$prod.name}</b><br /> {"Price"|translate}: {$prod.price|money}<br /><br />
	<li>
	{/foreach}
</ul>
</div>

</div>
