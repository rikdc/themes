<h1 class="postTitle">{$post.title}</h1>
<span class="postDate">{$post.updated|date_format:"%d/%m/%Y"}</span>
{$post.content}