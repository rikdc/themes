<table class="table table-striped">
  <thead>
     <tr>
       <th>{"Search Results"|translate}</th>
     </tr>
  </thead>
  <tbody>
  {foreach item=result from=$search.results}
   <tr>
    <td><a href="{$result.url}">{$result.name}</a></td>
   </tr>
   {/foreach}
  </tbody>
</table>
