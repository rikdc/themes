<h1>{$category.title}</h1>
{if (count($category.children) > 0)}
<ul class="nav nav-pills subright">
  <li class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        {"Sub Categories"|translate}
        <b class="caret"></b>
      </a>
    <ul class="dropdown-menu">
      {foreach from=$category.children item=category}
         <li><a href="{$category.url}">{$category.title}</a></li>
      {/foreach}
    </ul>
  </li>
</ul>
{/if}

<br style="clear: both;" />
<p class="categoriesdescription">{$category.description}</p>
<div class="products-search">
<ul class="thumbnails">
    
    {foreach from=$products item=product}
	    <li class="span3">
	    	<div class="thumbnail product-home">
	    	    {if $product.is_new == "1"}
	    	    	<div class="label label-important new-product">
		    	    	{"New Product!"|translate}
		    	    </div>
		    	{/if}
		    	
		    	{if ($product.was_price > 0)}
	    			<div class="product-sale">
		    	    	<span>{"SALE"|translate}</span>
		    	    </div>
	    		{else}{/if}
	    		
	    		<a class="product-image" href="products/{$product.slug}">
	    		
	    		{section name=image loop=$product.images}
       				{if $smarty.section.image.first}
    						{img_tag name=$product.images[image].name|product_image_url:'compact'}
       				{/if}
   			    {/section}
   			     		    		
	    		</a>
	    		<div class="caption"> <a href="products/{$product.slug}" class="product-title">{$product.name|truncate:35:"...":true}</a>    		
	    		{if ($product.was_price > 0)}
	    		<p class="price-was"><span>{$product.was_price|money}</span></p>
	    		{else}{/if}
    
	    		<div class="price">
	    		{if ($product.was_price > 0)}
	    		{"Price Now"|translate}:
	    		{else}
	    		{"Price"|translate}:
	    		{/if}
	    		 <span>{$product.price|money}</span></div>
	    		
	    		
	    		
	    		 
	    		
	    		{if $product.variations|@count gt 1}
	    		<p></p> <a href="products/{$product.slug}" class="btn">{"View More"|translate}</a>
	    		{else}
	    		<p></p> 
	    		<form method="post" action="/carts/add" id="form{$product.slug}">
	    		<input type="hidden" name="data[CartLine][quantity]" value="1" />
    		                      <input type="hidden" name="data[CartLine][product_line_id]" value="{$product.variations[0].id}" />
	    		<button class="btn btn-primary" name="add" type="submit"><i class="icon-left icon-shopping-cart icon-white"></i>{"Add to Cart"|translate}</button></form>
	    		{/if}
	    		
	    		</div>
	    	</div> 
	    </li>
	{/foreach}	    
    </ul>
   
<br style="clear: both;" />
{if count($products) == $site.settings.products_per_category_page}
	<p class="page-counter">{page_counter}</p>
    <div class="pagination pagination-centered"><ul><li class="current pages">{"Pages"|translate}: </li>{page_numbers hasNext="true" currentTag="span" tag="li span" separator=""}</ul></div>
{/if}
</div>
