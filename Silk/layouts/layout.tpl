<!DOCTYPE html>

<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
  <meta charset="utf-8" />
  <meta name="description" content="{$site.description}" />
  
  {if $template|contains:"product"}
  	<meta property="og:image" content="{section name=image loop=$product.images}{if $smarty.section.image.first}{$product.images[image].name|product_image_url}{/if}{/section}"/>
  {/if}
  
  <html xmlns:fb="http://ogp.me/ns/fb#">

  <title>{$title_for_layout}</title>
  <base href="{$baseurl}" />
  
  <!-- CSS -->
  {"bootstrap.css"|asset_url|stylesheet_tag}
  {"jquery.fancybox.css"|asset_url|stylesheet_tag}
  {"app.css"|asset_url|stylesheet_tag}
  {"glide.css"|asset_url|stylesheet_tag}
  
  <!-- Javascript -->
  <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
  {"bootstrap.js"|asset_url|script_tag}
  {"app.js"|asset_url|script_tag}
  {"jquery.fancybox.js"|asset_url|script_tag}
  {"jquery.glide.min.js"|asset_url|script_tag}

  <!-- IE Fix for HTML5 Tags -->
  <!--[if lt IE 9]>
    {literal}
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    {/literal}
  <![endif]-->

</head>
<body>
  {literal}
  	<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/is_IS/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
  {/literal}
  <!-- Header and Nav -->
 

  <div class="container">

  <div id="header">
    <div class="logo">
      <a href="/">{logo_tag}</a>
    </div>
    
    {if $site.facebook_url == ""}{else}
    	<div class="fb-header-like">
    		<div class="fb-like" data-href="{$site.facebook_url}" data-send="true" data-layout="button_count" data-width="200" data-show-faces="false"></div>
		</div>
	{/if}
    
    <div class="search-header">
	    <div class="input-append">
	    	<form action="/search" method="get" name="srchfrm" >
		    <input class="span2" placeholder="{"Keyword..."|translate}" id="appendedInputButton" name="q" size="16" type="text"><button class="btn btn-primary symbol" type="button">L</button>
	    	</form>
		</div>
    </div>

    
    <div class="cart">
	    <a href="/cart" rel="tooltip" title="{"View my Cart"|translate}">
		    <div class="cart-title">{"Shopping Cart"|translate}			    
			    
			    {if ($cart.cart_line_count > 1)}
			    	<span class="cart-items">{$cart.cart_line_count} {"Products"|translate}</span>
			    {/if}
			    {if ($cart.cart_line_count == 1)}
			    <span class="cart-items">{$cart.cart_line_count} {"Product"|translate}</span>
			    {/if}
			    {if ($cart.cart_line_count == 0)}
			    <span class="cart-items">0 {"Products"|translate}</span>
			    {/if}
			    
			    
		    </div>
		    <span class="cart-icon symbol">j</span>
	    </a>
    </div>
  </div>
  <br style="clear: both;" />
  <div id="navigation">
  	  <ul class="second-nav">
        	<li><a href="/">{"Home"|translate}</a></li>
      </ul>
	  <nav id="nav">
		  {categories_menu snippet="headernav.tpl"}
	  </nav>
	  <ul class="second-nav lastnav">
        	<li><a href="/contact">{"Contact Us"|translate}</a></li>
      </ul>
  </div>
  
  {if $isHome}
  	{snippet name="slideshow.tpl"}
  {/if}
  
  <!-- Three-up Content Blocks -->
  
  <div class="page-content">
  
  	{if $isHome}
  	{$content_for_layout}
  	{else}
  	{foreach item=crumbGroup from=$crumbs name=crumbGroup}{if $smarty.foreach.crumbGroup.first}<ul class="breadcrumb">{foreach item=crumb from=$crumbGroup name=crumbs}{if $smarty.foreach.crumbs.last}<li class="active">{$crumb.name}</li>{else}<li><a href="{$crumb.url}">{$crumb.name}</a> <span class="divider">/</span></li>{/if}{/foreach}</ul>{/if}{/foreach}
  	
  	<div id="content">
	{customer_messages}
	{$content_for_layout}
	</div>
	{/if}
	
	
    
    
  </div>
  
  </div>

  <!-- Footer -->
  
  <footer id="footer" class="container">
    <div>
      <div>
        <div>
          <p>
	          {"Copyright"|translate} &copy; <strong>{$siteName}</strong> {$yesterday|date_format:"%Y"} {"All Rights Reserved"|translate}<br />
	          <span><a href="mailto:{$site.email}">{$site.email}</a> - {"Telephone"|translate}: {$site.phone} - {$site.address}, {$site.postcode} {$site.city}</span>
          </p>
        </div>
        <div id="footer-nav">
          {pages_menu}
        </div>
      </div>
    </div> 
  </footer>
  {content_for_header}
{literal}
  <script>
    $('.slider').glide({
        arrows: false,
    });
</script>
{/literal}
</body>
</html>
