$(document).ready(function () {
    $("[rel=tooltip]").tooltip();
    
    $('.tabs a').click(function (e) {
	    e.preventDefault();
	    $(this).tab('show');
	})
	
	$(".fancybox").fancybox();
	$("#product-gallery a").fancybox();
});