<div id="home-slider">

<div class="slider">
    <ul class="slides">
        {foreach item=pic from=$galleries.slideshow}
            <li class="slide"><img src="{$pic.image.url}" title="" /></li>
        {/foreach}
    </ul>
</div>
        
</div>