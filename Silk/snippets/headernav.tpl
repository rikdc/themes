<ul class="nav">
	{foreach item=link from=$links}
		<li><a href="{$link.Category.url}" {if count($link.children) > 0}class="down-arrow"{/if}>{$link.Category.title}</a>
			{if count($link.children) > 0}
			<ul>
				{foreach item=level2 from=$link.children}
				<li><a href="{$level2.Category.url}">{$level2.Category.title}
						{if count($level2.children) > 0}
						<span class="symbol">></span>
						{/if}
				</a>
						
						
						
						{if count($level2.children) > 0}
							<ul>
								{foreach item=level3 from=$level2.children}
								<li><a href="{$level3.Category.url}">{$level3.Category.title}
									{if count($level3.children) > 0}
									<span class="symbol">></span>
									{/if}
								</a>
								
								
								
								{if count($level3.children) > 0}
									<ul>
										{foreach item=level4 from=$level3.children}
										<li><a href="{$level4.Category.url}">{$level4.Category.title}
											{if count($level4.children) > 0}
											<span class="symbol">></span>
											{/if}
										</a>
										
											{if count($level4.children) > 0}
												<ul>
													{foreach item=level5 from=$level4.children}
													<li><a href="{$level5.Category.url}">{$level5.Category.title}</a></li>
													{/foreach}
										        </ul>
										    {/if}
										
										</li>
										{/foreach}
									</ul>
								{/if}
								
								
								
								</li>
								{/foreach}
							</ul>
						{/if}
						
						
				</li>
				{/foreach}
			</ul>
			{/if}
			
	{/foreach}
</ul>